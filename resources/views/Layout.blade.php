<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <title>Adminstration Panel</title>

    </head>
    <body>
      
            <!-- Navbar content -->
            <nav class="navbar navbar-default">
                    <ul class="nav navbar-nav">
                      <li><a href="/Output/create">Enter a Quote</a></li>
                      <li><a href="/Output">Output</a></li>
                    </ul>
            </nav>
        <div class="container" >
    @yield('content')
        </div>
        </div>
    </body>
</html>
