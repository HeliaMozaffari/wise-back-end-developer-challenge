@extends('Layout')
@section('content')

<br>
   @if(count($quote)>0)
    
   @foreach ($quote  as $item)
      <div class="well">
         <h4 class="float-right">{{$item->Quote}}</h4>
         <h6>
              <img src="https://picsum.photos/150" alt="" class="float-right">
              Season:{{$item->Season}} |
              Episode:{{$item->Episode}}
         </h6>
              <div class="Delete">
                  {!!Form::open(['action'=>['QuoteController@destroy',$item->id], 'method' => 'Post', 'class'=>'pull-right'])!!}
                  {{Form::hidden('_method','Delete')}}
                  {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
                  {!!Form::close()!!}
              </div>
              <a href="/Output/{{$item->id}}/edit">  <button type="button" class="btn btn-warning">Edit</button></a>
      </div>
   @endforeach
   @else 
   <p>No Quotes found </p>

   @endif
   
 
@endsection
        
   

