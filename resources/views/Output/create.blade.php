@extends('Layout')
@section('content')
    <div class="text-center">
    {!!Form::open(['action'=>'QuoteController@store', 'method' => 'Post'])!!}
   
        <div class="form-group">
          {!!Form::text('Season','',['class'=>'form-control' , 'placeholder'=>'Season'])!!}
          </div>

      
        <div class="form-group">
          {!!Form::text('Episode','',['class'=>'form-control', 'placeholder'=>'Episode'])!!}
          </div>

        
          <div class="form-group">
            {!!Form::textarea('Quote','',['class'=>'form-control', 'placeholder'=>'Quote'])!!}
            </div>

            {!! Form::submit('submit', ['class' => 'btn btn-primary']) !!}
     {!! Form::close() !!}
    </div>

@endsection
