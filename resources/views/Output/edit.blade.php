@extends('Layout')
@section('content')
    <div class="text-center">
    {!!Form::open(['action'=>['QuoteController@update',  $quote->id], 'method' => 'Post'])!!}
   
        <div class="form-group">
          {!!Form::text('Season',$quote->Season,['class'=>'form-control' , 'placeholder'=>'Season'])!!}
          </div>

      
        <div class="form-group">
          {!!Form::text('Episode',$quote->Episode,['class'=>'form-control', 'placeholder'=>'Episode'])!!}
          </div>

        
          <div class="form-group">
            {!!Form::textarea('Quote',$quote->Quote,['class'=>'form-control', 'placeholder'=>'Quote'])!!}
            </div>

            {!! Form::hidden('_method', 'PUT') !!}
            {!! Form::submit('submit', ['class' => 'btn btn-primary']) !!}
     {!! Form::close() !!}
    </div>

@endsection
