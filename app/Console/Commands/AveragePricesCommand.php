<?php

namespace App\Console\Commands;

use App\Price;
use Illuminate\Console\Command;
use DB;

class AveragePricesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'States:AveragePrice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the average prices for each of the food items by state';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    
       
         $array=DB::table('ap_copi')
            ->join('ap_states', 'ap_copi.state_id', '=', 'ap_states.id')
            ->selectRaw('ap_states.state as state,avg(steak)as Steak,avg(grnd_beef)as Beef,avg(sausage) as Suasage, avg(fry_chick) as Fry_chick')
            ->groupBy('state_id', 'state')->get();
                foreach ($array as $result) 
                {
                    echo $result->state,": Steak(",$result->Steak,")","Ground Beef(",$result->Beef,")"," Sausage(",$result->Suasage,")","Fried Chicken(",$result->Fry_chick,")","\n";
                }
    
         
        
    }
}