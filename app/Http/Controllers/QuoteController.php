<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\quote;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $quotes = quote::all();
        return view('Output.index') -> with('quote', $quotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Output.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this -> validate ($request, [
       'Quote' => 'required',
       'Season' => 'required',
       'Episode' => 'required'
        ]);
    

        $quotes = new quote;
        $quotes->Quote = $request->Quote;
        $quotes->Season = $request->Season;
        $quotes->Episode = $request->Episode;
        $quotes->save();

        return redirect('/Output');
    

        
    

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$quotes = quote::find($id);
        //return view('edit')->with('quote', $quotes);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quote = quote::find($id);
        return view('Output.edit')->with('quote', $quote);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate ($request, [
            'Quote' => 'required',
            'Season' => 'required',
            'Episode' => 'required'
             ]);
         
     
             $quotes = quote::find($id);
             $quotes->Quote = $request->input('Quote');
             $quotes->Season = $request->input('Season');
             $quotes->Episode = $request->input('Episode');
             $quotes->save();
     
             return redirect('/Output');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotes = quote::find($id);
        $quotes->delete();
        return redirect('/Output');

    }
}


//